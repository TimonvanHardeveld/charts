graphsArr = ['restAfval','scheidingsAfval','afvalStoffenHeffing'];

$("#gemeenten").on("change",function(){
    gemeente = $(this).val();
    updateGraph(gemeente);
});

//https://developer.snapappointments.com/bootstrap-select/options/
$('#gemeenten').selectpicker({
    liveSearch: true,
    liveSearchPlaceholder: "Zoeken",
    noneSelectedText: "Selecteer uw gemeente",
    noneResultsText: "Er zijn geen resultaten {0}"
});