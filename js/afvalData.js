// afvalData = {
//     labels:["2013", "2014", "2015", "2016", "2017"],
//     gemeentes:{        
//         "Aalsmeer": {
//             restAfval:[203, 199, 204, 204, 171],
//             scheidingsAfval:[60, 59, 59, 58, 64],
//             afvalStoffenHeffing:[240, 230, 210, 200, 196],
//             organisatie: "Gemeente Aalsmeer"
//         },
//         "Amsterdam": {
//             restAfval:[355, 352, 323, 327, 266],
//             scheidingsAfval: [56, 22, 33, 59, 62],
//             afvalStoffenHeffing:[240, 230, 220, 200, 196],
//             organisatie: "Gemeente Amsterdam"
//         }           
//     },
//     min: {
//         restAfval: 171,
//         scheidingsAfval: 20,
//         afvalStoffenHeffing: 196
//     },
//     max: {
//         restAfval: 356,
//         scheidingsAfval: 70,
//         afvalStoffenHeffing: 246
//     },
//     average:{
//         restAfval:[222, 217, 211, 201, 182],
//         scheidingsAfval:[56, 58, 33, 59, 62],
//         afvalStoffenHeffing:[246, 242, 242, 239, 235]
//     }
// }

var afvalData;
//Percentage offset on max and min
var extremesOffset = 20;

$(document).ready(function(){   
    convertCsvFile();
});

function convertCsvFile(){
    getcsvData("csv/afvalData.csv",function(data){
        afvalData = convertDataToGraph(data);
        InitChart();
        $(".selectpicker").selectpicker('refresh');
    });
}

// Do the ajax call to convert the csv to an array of objects
function getcsvData(url, callback){
    $.ajax({
        type: "GET",  
        url: url,
        dataType: "text",     
        success: function(response)  
        {         
            if(response == null || response === "")
                return console.log("Er is geen data gevonden");
            data = $.csv.toObjects(response, {separator:";"});
            callback(data);
        },
        error: function(){
            console.log("Er gaat iets fout");
        }
      });
}

// Convert the array of objects to graphdata
function convertDataToGraph(data){
    len = data.length;
    var res = {
        labels: getLabels(data[0]),
        gemeentes : {}, 
        min : {
            restAfval: getMinMaxValue(data,"Rest", true),
            scheidingsAfval: getMinMaxValue(data,"Scheiding", true),
            afvalStoffenHeffing: getMinMaxValue(data,"Afvalstoffen", true)
        },
        max : {
            restAfval: getMinMaxValue(data,"Rest", false),
            scheidingsAfval: getMinMaxValue(data,"Scheiding", false),
            afvalStoffenHeffing: getMinMaxValue(data,"Afvalstoffen", false)
        }, 
        average : getCityInfo(data[0], true)
    };

    //Fill the result object with all the cities
    for (var i = 0; i < len; i++) {
        obj = data[i];
        res.gemeentes[obj["Korte Naam"]] = getCityInfo(obj);
    }

    return res;
}

// Fill the city with the correct info
function getCityInfo(city, isAverage = false){
    var result = {
        restAfval : getDataValues(city, "Rest", isAverage),
        scheidingsAfval : getDataValues(city, "Scheiding", isAverage),
        afvalStoffenHeffing : getDataValues(city, "Afvalstoffen", isAverage)
    };
    if(!isAverage)
    {
        result.organisatie =  city.Organisatie;
    }
    return result;
}

// Get the properties that contain data and put them in an array
function getDataValues(city, indexType, isAverage){
    return Object.values(getDataProperties(city, indexType, isAverage));
}

// Get all the properties that start with an indexType (Restafval e.d.) and filter if average is needed
function getDataProperties(city, indexType, isAverage){
    return Object.keys(city).filter(function(k) {
        return k.indexOf(indexType) == 0 && k.includes("gemiddeld") == isAverage;
    }).reduce(function(newData, k) {
        newData[k] = city[k];
        return newData;
    }, {});
}

// Determine what labels should be displayed under the graph
function getLabels(city){
    var z = getDataProperties(city, "Restafval", false);
    z= Object.getOwnPropertyNames(z);
    return z.map(x=> x.replace("Restafval ",""));
}

// Get either the min or the max values from all cities in the respective indexarea
function getMinMaxValue(data, indexType, isMin){
    var z = data.map(x=>getDataValues(x, indexType, false));
    var min = getMinFromArrays(z);
    var max = getMaxFromArrays(z);
    var offset = (max - min) * 0.01 * extremesOffset;
    var result = isMin ? min - offset  : max + offset;
    return result;
}

function getMinFromArrays(arrays){
    return arrays.reduce((min, p) => Math.min.apply(null, p) < min ? Math.min.apply(null, p) : min, Math.min.apply(null,arrays[0]));
}

function getMaxFromArrays(arrays){
    return arrays.reduce((max, p) => Math.max.apply(null, p) > max ? Math.max.apply(null, p) : max, Math.max.apply(null,arrays[0]));
}