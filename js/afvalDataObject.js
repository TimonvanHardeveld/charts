/*
graphData = {
    labels:["2013", "2014", "2015", "2016", "2017"],
    gemeentes:{        
        "Aalsmeer": {
            restAfval:[203.1, 199.7, 204, 204.3, 171.17],
            scheidingsAfval = [60, 59, 59, 58, 64],
            afvalStoffenHeffing = [240, 230, 210, 200, 196],
            organisatie: "Gemeente Aalsmeer"
        },
        "Amsterdam": {
            restAfval:[355.5, 352.7, 323, 327.6, 266.29],
            scheidingsAfval = [16, 17, 19, 18, 27],
            afvalStoffenHeffing = [240, 230, 210, 200, 196],
            organisatie: "Gemeente Amsterdam"
        }           
    },
    min: {
        restAfval: 171,
        scheidingsAfval: 16,
        afvalStoffenHeffing: 196
    },
    max: {
        restAfval: 356,
        scheidingsAfval: 65,
        afvalStoffenHeffing: 240
    },
    average:{
        restAfval:{
            landelijk: [222.36, 217.34, 211.34, 201.91, 182.04],
            hoogbouwklasses:{            
                A: [222.36, 217.34, 211.34, 201.91, 182.04],
                B: [222.36, 217.34, 211.34, 201.91, 182.04],
                C: [222.36, 217.34, 211.34, 201.91, 182.04],
                D: [222.36, 217.34, 211.34, 201.91, 182.04]
            }
        },
        scheidingsafval:{
            landelijk: [55.74, 56.92, 58.1, 60.2, 64.12],
            hoogbouwklasses:{            
                A: [222.36, 217.34, 211.34, 201.91, 182.04],
                B: [222.36, 217.34, 211.34, 201.91, 182.04],
                C: [222.36, 217.34, 211.34, 201.91, 182.04],
                D: [222.36, 217.34, 211.34, 201.91, 182.04]
            }
        },        
        afvalStoffenHeffing:{
            landelijk:[246, 242, 242, 239, 235],
            hoogbouwklasses:{            
                A: [222.36, 217.34, 211.34, 201.91, 182.04],
                B: [222.36, 217.34, 211.34, 201.91, 182.04],
                C: [222.36, 217.34, 211.34, 201.91, 182.04],
                D: [222.36, 217.34, 211.34, 201.91, 182.04]
            }
        }
    }
}
*/