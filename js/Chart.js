graphsArr = ['restAfval','scheidingsAfval','afvalStoffenHeffing'];

let options = {
    responsive:false,
    maintainAspectRatio: false,
    tooltips: {
        custom: function(tooltip) {
          if (!tooltip) return;
          // disable displaying the color box;
          tooltip.displayColors = false;
        },
        callbacks: {
          // use label callback to return the desired label
          label: function(tooltipItem, data) {
            return tooltipItem.xLabel + " :je moeder" + tooltipItem.yLabel;
          },
          // remove title
          title: function(tooltipItem, data) {
            return;
          }
        }
      }
    
};

var graphs = {};

function InitChart(){
    for (i = 0; i < graphsArr.length; i++) {         
        graph = document.getElementById(graphsArr[i]).getContext('2d');
        graphs[graphsArr[i]] = new Chart(graph, {
            options:options,
            type: 'line',
            data: {
                labels: afvalData.labels,
                datasets: [{  
                        fill:false,
                        // label: 'Afvalstofheffing',
                        borderColor: [
                            '#E72158',
                        ],
                        //set line width
                        borderWidth: 7,
                        //set curve line
                        lineTension: 0.1,
                        data: afvalData.average[graphsArr[i]]
                }]
            },
            options: {
                animation:{
                    easing:"linear"
                },
                legend: {
                    display:false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        //remove y axis
                        display:false,
                        ticks: {
                            beginAtZero:false,
                            min:afvalData.min[graphsArr[i]] ,
                            max:afvalData.max[graphsArr[i]] + 3
                        }
                    }]
                }
            }
        });
    }

        //set selectbox
        function setSelectGemeentes(){    
            var output = [];
            output.push('<option />');            
            for(gemeente in afvalData.gemeentes) {
                output.push('<option value="'+ gemeente +'">'+ gemeente +'</option>');
            }
            //is for only one push to DOM
            $('#gemeenten').html(output.join(''));
        }    
        setSelectGemeentes();
}

function updateGraph(gemeente){
    for (i = 0; i < graphsArr.length; i++) {
        var datasetGemeente ={
            fill:false,
            borderColor:[
                '#000000',
            ],
            //set line width
            borderWidth: 7,
            //set curve line
            lineTension: 0.1
        };
        if(graphs[graphsArr[i]].data.datasets.length == 2){
            graphs[graphsArr[i]].data.datasets.splice(1,1);
        }

        datasetGemeente.data = afvalData.gemeentes[gemeente][graphsArr[i]];

        graphs[graphsArr[i]].data.datasets.push(datasetGemeente);
        graphs[graphsArr[i]].update();
    }			
}